﻿using System.Web;
using System.Web.Optimization;

namespace PMSPrototypeUI
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/moment.js",
                      "~/Scripts/fullcalendar.resource.js",
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/bootstrap-datepicker.js",
                      "~/Scripts/bootstrap-multiselect.js",
                      "~/Scripts/daterangepicker.js",
                      "~/Scripts/jquery-ui.js",
                      "~/Scripts/jquery.qtip.js",
                      "~/Scripts/respond.js",
                      "~/Scripts/script.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/core.css",
                      "~/Content/bootstrap.css",
                      "~/Content/bootstrap-multiselect.css",
                      "~/Content/datepicker.css",
                      "~/Content/daterangepicker.css",
                      "~/Content/jquery-ui.css",
                      "~/Content/jquery.qtip.css",
                      "~/Content/fullcalendar.resource.css",
                      "~/Content/glyphicons.css",
                      "~/Content/site.css",
                      "~/Content/skin.css"));

            // Set EnableOptimizations to false for debugging. For more information,
            // visit http://go.microsoft.com/fwlink/?LinkId=301862
            //BundleTable.EnableOptimizations = true;
        }
    }
}
