﻿function getViewport() {
    var viewPortWidth;
    var viewPortHeight;

    // the more standards compliant browsers (mozilla/netscape/opera/IE7) use window.innerWidth and window.innerHeight
    if (typeof window.innerWidth != 'undefined') {
        viewPortWidth = window.innerWidth,
        viewPortHeight = window.innerHeight
    }
        // IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)
    else if (typeof document.documentElement != 'undefined'
    && typeof document.documentElement.clientWidth !=
    'undefined' && document.documentElement.clientWidth != 0) {
        viewPortWidth = document.documentElement.clientWidth,
        viewPortHeight = document.documentElement.clientHeight
    }

        // older versions of IE
    else {
        viewPortWidth = document.getElementsByTagName('body')[0].clientWidth,
        viewPortHeight = document.getElementsByTagName('body')[0].clientHeight
    }
    return [viewPortWidth, viewPortHeight];
}
function isOverlapping(event) {
    //var array = $('.apptCalMod').fullCalendar('clientEvents');
    //for (i = 0; i < array.length; i++) {
    //    if (array[i].id != event.id) {
    //        if (!(array[i].start.format() >= event.end.format() || array[i].end.format() <= event.start.format())) {
    //            console.log('Item: ' + array[i].id + ' Start: ' + array[i].start + ' End: ' + array[i].end);
    //            console.log('Event: ' + event.id + ' Start: ' + event.start + ' End: ' + event.end);
    //            return true;
    //        }
    //    }
    //}
    var start = new Date(event.start);
    var end = new Date(event.end);

    var overlap = $('.apptCalMod').fullCalendar('clientEvents', function (ev) {
        if (ev == event)
            return false;
        var estart = new Date(ev.start);
        var eend = new Date(ev.end);

        return (Math.round(estart) / 1000 < Math.round(end) / 1000 && Math.round(eend) > Math.round(start));
    });

    if (overlap.length) {
        //either move this event to available timeslot or remove it
        console.log('over lapping');
        return true;
    }
    return false;
}
$(function () {
    $.widget("custom.catcomplete", $.ui.autocomplete, {
        _create: function () {
            this._super();
            this.widget().menu("option", "items", "> :not(.ui-autocomplete-category)");
        },
        _renderMenu: function (ul, items) {
            var that = this,
              currentCategory = "";
            $.each(items, function (index, item) {
                var li;
                if (item.category != currentCategory) {
                    ul.append("<li class='ui-autocomplete-category'>" + item.category + "</li>");
                    currentCategory = item.category;
                }
                li = that._renderItemData(ul, item);
                if (item.category) {
                    li.attr("aria-label", item.category + " : " + item.label);
                }
            });
        }
    });

    var treatmentdata = [
      { label: "(012) anders", category: "Minor" },
      { label: "(121) andreas", category: "Minor" },
      { label: "(122) antal", category: "Minor" },
      { label: "(144) annhhx10", category: "Minor" },
      { label: "(022) annk K12", category: "Major" },
      { label: "(021) annttop C13", category: "Major" },
      { label: "(013) anders andersson", category: "Surgical" },
      { label: "(014) andreas andersson", category: "Surgical" },
      { label: "(011) andreas johnson", category: "Surgical" }
    ];

    var peopledata = [
      { label: "Mr Smith Black (19/08/1980)", category: "Patient" },
      { label: "Mrs John Black (19/08/1980)", category: "Patient" },
      { label: "Mr Junior Black (19/08/1980)", category: "Patient" },
      { label: "Dr Elizabeth John", category: "Dentist" },
      { label: "Dr Peter Joe", category: "Dentist" },
      { label: "Dr Mary Smith", category: "Employee" },
    ];

    var resourcedata = [
        {
            id: 'resource1',
            name: 'Dr Elizabeth Smith',
        },
        {
            id: 'resource2',
            name: 'Dr Alan Black',
        },
        {
            id: 'resource3',
            name: 'Dr Robert White',
        }
    ];

    var eventdata = [
        {
            id: 1,
            resources: 'resource1',
            title: 'Ms Julia Mclean \n Dr Elizabeth Smith \n 90 mins \n Extraction',
            start: moment({hour: 16, minute: 0}).add('day', 1),//'2014-09-09T16:00:00',
            end: moment({ hour: 17, minute: 0 }).add('day', 1),//'2014-09-09T17:00:00',
            backgroundColor: 'rgb(164, 189, 252)',
            type: 'Extraction',
            user: 'Ms Julia Mclean',
            state: {
                title: 'Arrived',
                description: 'Arrived',
                time: moment({ hour: 16, minute: 11 }).add('day', 1)
            },
            status: [
                {
                    title: 'CN',
                    color: '#7DC977',
                    description: 'Confirmed',
                },
                {
                    title: 'SS',
                    color: '#E66064',
                    description: 'SMS Sent',
                },
                {
                    title: 'NA',
                    color: '#7FA3F2',
                    description: 'No Answer',
                },
                {
                    title: 'NP',
                    color: '#E4C2FF',
                    description: 'New Patient',
                }
            ]
        },
        {
            id: 2,
            resources: 'resource1',
            title: 'Ms Julia Mclean \n Dr Elizabeth Smith \n 90 mins \n General Clearning',
            start: moment({ hour: 10, minute: 30 }).add('day', 1),//'2014-09-12T10:30:00',
            end: moment({ hour: 12, minute: 0 }).add('day', 1),//'2014-09-12T12:00:00',
            description: 'Dr Elizabeth Smith 1 hr and 30 mins',
            backgroundColor: 'rgb(81, 183, 73)',
            type: 'General Clearning',
            user: 'Ms Julia Mclean',
            state: {
                title: 'In',
                description: 'Checked In',
                time: moment({ hour: 10, minute: 31 }).add('day', 1)
            },
            status: [
                {
                    title: 'CN',
                    color: '#7DC977',
                    description: 'Confirmed',
                },
                {
                    title: 'SS',
                    color: '#E66064',
                    description: 'SMS Sent',
                },
                {
                    title: 'NA',
                    color: '#7FA3F2',
                    description: 'No Answer',
                },
                {
                    title: 'NP',
                    color: '#E4C2FF',
                    description: 'New Patient',
                }
            ]
        },
        {
            id: 9991,
            resources: 'resource1',
            title: 'Lunch \n Dr Elizabeth Smith',
            backgroundColor: '#dd2b30',
            start: moment({ hour: 12, minute: 0 }).add('day', 1),//'2014-09-12T12:00:00',
            end: moment({ hour: 13, minute: 0 }).add('day', 1),//'2014-09-12T13:00:00',
            user: 'Ms Elizabeth Smith',
        },
        {
            id: 4,
            resources: 'resource1',
            title: 'Mr Peter Mclean \n  Dr Elizabeth Smith  \n 60 mins',
            start: moment({ hour: 14, minute: 30 }).add('day', 1),//'2014-09-12T14:30:00',
            end: moment({ hour: 15, minute: 30 }).add('day', 1),//'2014-09-12T15:30:00',
            backgroundColor: 'rgb(219, 173, 255)',
            type: 'General Clearning',
            user: 'Ms Peter Mclean',
            state: {
                title: 'Out',
                description: 'Checked Out',
                time: moment({ hour: 14, minute: 34 }).add('day', 1)
            },
            status: [
                {
                    title: 'CN',
                    color: '#7DC977',
                    description: 'Confirmed',
                },
                {
                    title: 'SS',
                    color: '#E66064',
                    description: 'SMS Sent',
                },
                {
                    title: 'NA',
                    color: '#7FA3F2',
                    description: 'No Answer',
                },
                {
                    title: 'NP',
                    color: '#E4C2FF',
                    description: 'New Patient',
                }
            ]
        },
        {
            id: 5,
            resources: 'resource1',
            title: 'Mr Peter Mclean \n  Dr Elizabeth Smith  \n 60 mins \n  General Clearning',
            start: moment({ hour: 17, minute: 30 }).add('day', 1),//'2014-09-12T17:30:00',
            end: moment({ hour: 18, minute: 0 }).add('day', 1),//'2014-09-12T18:00:00',
            backgroundColor: 'rgb(81, 183, 73)',
            type: 'General Clearning',
            user: 'Ms Peter Mclean',
            status: [
                {
                    title: 'CN',
                    color: '#7DC977',
                    description: 'Confirmed',
                },
                {
                    title: 'SS',
                    color: '#E66064',
                    description: 'SMS Sent',
                },
                {
                    title: 'NA',
                    color: '#7FA3F2',
                    description: 'No Answer',
                },
                {
                    title: 'NP',
                    color: '#E4C2FF',
                    description: 'New Patient',
                }
            ]
        },
        {
            id: 9992,
            resources: 'resource3',
            title: 'Lunch \n  Dr Robert White',
            backgroundColor: '#dd2b30',
            start: moment({ hour: 12, minute: 0 }).add('day', 1),//'2014-09-12T12:00:00',
            end: moment({ hour: 13, minute: 0 }).add('day', 1),//'2014-09-12T13:00:00',
            user: 'Dr Robert White',
        },
        {
            id: 6,
            resources: 'resource3',
            title: 'Ms Peter Mclean \n  Dr Robert White  \n 60 mins \n  Crown',
            start: moment({ hour: 13, minute: 0 }).add('day', 1),//'2014-09-12T13:00:00',
            end: moment({ hour: 14, minute: 0 }).add('day', 1),//'2014-09-12T14:00:00',
            backgroundColor: 'rgb(84, 132, 237)',
            type: 'Crown',
            user: 'Ms Peter Mclean',
            status: [
                {
                    title: 'CN',
                    color: '#7DC977',
                    description: 'Confirmed',
                },
                {
                    title: 'SS',
                    color: '#E66064',
                    description: 'SMS Sent',
                },
                {
                    title: 'NA',
                    color: '#7FA3F2',
                    description: 'No Answer',
                },
            ]
        },
        {
            id: 9993,
            resources: 'resource2',
            title: 'Lunch \n  Dr Alan Black',
            backgroundColor: '#dd2b30',
            start: moment({ hour: 12, minute: 0 }).add('day', 1),//'2014-09-12T12:00:00',
            end: moment({ hour: 13, minute: 0 }).add('day', 1),//'2014-09-12T13:00:00',
            user: 'Dr Alan Black',
        },
        {
            id: 8,
            resources: 'resource2',
            title: 'Ms Julia Mclean \n Dr Alan Black \n 90 mins \n General Clearning',
            start: moment({ hour: 10, minute: 30 }).add('day', 1),//'2014-09-12T10:30:00',
            end: moment({ hour: 12, minute: 0 }).add('day', 1),//'2014-09-12T12:00:00',
            description: 'Dr Elizabeth Smith 1 hr and 30 mins',
            backgroundColor: 'rgb(81, 183, 73)',
            type: 'General Clearning',
            user: 'Ms Julia Mclean',
            status: [
                {
                    title: 'CN',
                    color: '#7DC977',
                    description: 'Confirmed',
                },
                {
                    title: 'SS',
                    color: '#E66064',
                    description: 'SMS Sent',
                },
                {
                    title: 'NP',
                    color: '#E4C2FF',
                    description: 'New Patient',
                }
            ]
        },
        {
            id: 7,
            resources: 'resource2',
            title: 'Mr Peter Junior \n  Dr Alan Black  \n 60 mins \n  General Clearning',
            start: moment({ hour: 08, minute: 0 }).add('day', 2),// '2014-09-13T08:00:00',
            end: moment({ hour: 09, minute: 0 }).add('day', 2),// '2014-09-13T08:00:00',
            backgroundColor: 'rgb(81, 183, 73)',
            type: 'General Clearning',
            user: 'Mr Peter Junior',
            status: [
                {
                    title: 'CN',
                    color: '#7DC977',
                    description: 'Confirmed',
                },
                {
                    title: 'SS',
                    color: '#E66064',
                    description: 'SMS Sent',
                },
            ]
        }
    ];

    $(".auto-treatment").catcomplete({
        source: treatmentdata
    });

    $(".auto-quicksearch").catcomplete({
        source: peopledata
    });

    var padding = $('.navbar').height() + $('.header').outerHeight();
    $('.tbl-grp').height(getViewport()[1] - padding);
    window.onresize = function (event) {
        $('.tbl-grp').height(getViewport()[1] - padding);
    };

    var tooltip = $('<div/>').qtip({
        id: 'fullcalendar',
        prerender: true,
        content: {
            text: ' ',
            title: {
                button: false
            }
        },
        position: {
            my: 'top left',
            at: 'bottom left',
            target: 'mouse',
            viewport: $('.apptCalMod'),
            container: $('.apptCalMod'),
            adjust: {
                mouse: true,
                y: 20
            },
            //effect: function (api, pos, viewport) {
            //     "this" refers to the tooltip
            //    $(this).animate(pos, {
            //        duration: 600,
            //        easing: 'linear',
            //        queue: false // Set this to false so it doesn't interfere with the show/hide animations
            //    });
            //}
        },
        show: false,
        hide: true,
        style: 'qtip-bootstrap'
    }).qtip('api');

    var disableDays = [0];
    $('.apptCalMod').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'agendaWeek,resourceDay'
        },
        titleFormat: {
            week: "ddd MMM D YYYY", // Sep 13 2009
            day: 'dddd MMM D YYYY'  // September 8 2009
        },
        defaultView: 'resourceDay',
        minTime: '08:00:00',
        maxTime: '19:00:00',
        //defaultDate: '2014-09-12',
        firstDay: 1,
        slotDuration: '00:15:00',
        contentHeight: getViewport()[1] - padding - 67,
        allDaySlot: false,
        slotEventOverlap: false,
        selectable: true,
        selectHelper: true,
        //hiddenDays: [0],
        editable: true,
        forceEventDuration: true,
        eventLimit: true, // allow "more" link when too many events
        dayRender: function (date, cell) {
            var today = new Date();
            //console.log('test');
            //console.log(cell);

            if (date < today) {
                //$(cell).addClass('fc-state-highlight');
                console.log(cell);
            } else {
                //$(cell).removeClass('fc-state-highlight');
            }
        },
        dayClick: function (date, jsEvent, view) {
            var today = new Date();

            if (date < today) {
                //TRUE Clicked date smaller than today + daysToadd
                //console.log("You cannot book on this day!" + date.format());
                view.unselect();
            } else {
                //FLASE Clicked date larger than today + daysToadd
                //console.log("Excellent choice! We can book today..");
            }

        },
        eventRender: function (event, element) {
            var title = $('<b/>', {
                html: event.start.format('h:mm a') + ' - ' + event.end.format('h:mm a')
            });
            var content = $('<p/>', {
                html: '<b>Patient:</b> ' + event.user
            });
            if (event.type && event.type != '') {
                content.append('<br/><b>Type:</b> ' + event.type);
            }
            var label;
            if (event.status && event.status.length > 0) {
                label = $('<ul/>', {
                    'class': 'statlabel',
                }).appendTo(element.find('.fc-event-title'));
            }
            if (label != null && event.state != null) {
                var statecls = '';
                if (event.state.title == 'In')
                    statecls = 'log_in';
                else if (event.state.title == 'Out')
                    statecls = 'new_window_alt';
                else if (event.state.title == 'Arrived')
                    statecls = 'clock';

                if (statecls != '') {
                    $('<li/>', {
                        html: '<span class="glyphicons ' + statecls + '"></span>'
                    }).appendTo(label);

                    $('<div/>', {
                        'class': 'pushT pullT5 border-gry-t',
                        html: '<b>State:</b>'
                    }).appendTo(content);
                    var state = $('<ul/>').appendTo(content);
                    $('<li/>', {
                        html: '<span class="glyphicons ' + statecls + ' pushR5"></span>' + event.state.description + ' at ' + event.state.time.format('h:mm a')
                    }).appendTo(state);
                    console.log(event.state.time.toString());
                }
            }
            if (label != null && event.status && event.status.length > 0) {
                $('<div/>', {
                    'class': 'pushT pullT5 border-gry-t',
                    html: '<b>Status:</b>'
                }).appendTo(content);

                var status = $('<ul/>').appendTo(content);
                for (i = 0; i < event.status.length; i++) {
                    $('<li/>', {
                        html: '<span style="color:' + event.status[i].color + '"' + '>' + event.status[i].title + '</span> ' + event.status[i].description
                    }).appendTo(status);
                    $('<li/>', {
                        html: '<span style="color:' + event.status[i].color + '"' + '>' + event.status[i].title + '</span>'
                    }).appendTo(label);
                }
            }

            element.qtip({
                overwrite: true,
                content: {
                    title: title,
                    text: content,
                    button: true
                },
                position: {
                    my: 'top left',
                    at: 'bottom right',
                    target: $(element),
                    adjust: {
                        y: -2,
                        x: 10,
                    },
                    viewport: $(window),
                    container: $('.apptCalMod'),
                },
                show: {
                    event: 'click',
                    solo: true,
                },
                hide: {
                    event: 'click'
                },
                style: {
                    classes: 'qtip-bootstrap',
                },
            });
            
            console.log(element.width());
            //element.find('.fc-event-title').append('test');
        },
        eventClick: function (event, jsEvent, view) {
            //var content = '<h3>' + event.title + '</h3>' +
            //    '<p><b>Start:</b> ' + event.start + '<br />' +
            //    '<p><b>End:</b> ' + event.end + '</p>';

            //console.log(jsEvent);
            //tooltip.set({
            //    'content.text': content
            //}).reposition(jsEvent, true).show(jsEvent);
        },
        //eventMouseover: function (event, jsEvent, view) {
        //    var content = '<h3>' + event.title + '</h3>' +
        //        '<p><b>Start:</b> ' + event.start + '<br />' +
        //        '<p><b>End:</b> ' + event.end + '</p>';

        //    console.log(jsEvent);
        //    tooltip.hide().set({
        //        'content.text': content
        //    }).reposition(jsEvent, true).show(jsEvent);
        //},
        //eventMouseout: function (event, jsEvent, view) {
        //    tooltip.hide();
        //},
        select: function (start, end, jsEvent, view) {
            var today = new Date();
            console.log('You"ve selented ' + start + ' ' + end);

            if (start < today) {
                view.unselect();
            }
            else {
                // Its a right date
                // Do something
            }
        },
        eventDragStop: function (event, jsEvent, ui, view) {
            if (isOverlapping(event)) {
                //revertFunc();
            };
        },
        resources: resourcedata,
        events: eventdata,
    });

    //avoid conflict with jquery date picker
    var datepicker = $.fn.datepicker.noConflict();
    $.fn.bootdatepicker = datepicker;

    $('.apptCalMod .fc-header .fc-header-title h2').bootdatepicker({
        todayHighlight: true,
        autoclose: true,
    }).on('show', function (e) {
        var caldate = $('.apptCalMod').fullCalendar('getDate');
        $(this).bootdatepicker('update', caldate.toDate());
    }).on('changeDate', function (e) {
        $('.apptCalMod').fullCalendar('gotoDate', e.date);
    });
    $('input.dpicker').bootdatepicker({
        format: "dd/M/yyyy",
        todayHighlight: true,
        autoclose: true,
        orientation: 'right',
    }).on('changeDate', function (e) {
        $(this).val(e.date);
    });

    var currentFiscalYearStart;
    var currentFiscalYearEnd;
    var lastFiscalYearStart;
    var lastFiscalYearEnd;

    if (moment().quarter() < 3) {
        currentFiscalYearStart = moment().subtract('year', 1).month('July').startOf('month');
        currentFiscalYearEnd = moment().month('June').endOf('month');
        lastFiscalYearStart = moment().subtract('year', 2).month('July').startOf('month');
        lastFiscalYearEnd = moment().subtract('year', 1).month('June').endOf('month');
    } else {
        currentFiscalYearStart = moment().month('July').startOf('month');
        currentFiscalYearEnd = moment().add('year', 1).month('June').endOf('month');
        lastFiscalYearStart = moment().subtract('year', 1).month('July').startOf('month');
        lastFiscalYearEnd = moment().month('June').endOf('month');
    };
    $('#reportrange').daterangepicker({
        opens: 'left',
        ranges: {
            'Today': [moment(), moment()],
            'Last 30 Days': [moment().subtract('days', 29), moment()],
            'Last 3 Months': [moment().subtract('months', 3), moment()],
            'Last 6 Months': [moment().subtract('months', 6), moment()],
            'This Financial Year': [currentFiscalYearStart, currentFiscalYearEnd],
            'Last Financial Year': [lastFiscalYearStart, lastFiscalYearEnd]
        },
        startDate: moment().subtract('days', 29),
        endDate: moment()
    },
    function(start, end) {
        $('#reportrange span:first').html(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
    });
    $('.main input.form-control').tooltip();
    $('.multiselect').multiselect({
        dropRight: true,
        //enableFiltering: true,
        filterBehavior: 'both',
        buttonWidth: '100%',
        buttonContainer: '<span class="dropdown" />',
        //nonSelectedText: 'None Selected',
        numberDisplayed: 1,
        maxHeight: 120,
        buttonTitle: function (options, select) {
            var selected = '';
            options.each(function () {
                selected += $(this).text() + ', ';
            });
            return selected.substr(0, selected.length - 2);
        },
});
});